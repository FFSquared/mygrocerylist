<?php 
include 'bootstrap.php';

class LoginController extends Controller {
    
    protected $layoutTemplate = 'layout-no-menu'; 
    
    protected $template  = 'login'; 
    
    protected $variables = array(
        'error_message' => '',
    ); 
    
    public function preRender() {
        global $session; 
        
        // If form was submitted 
        if (!empty($_POST)) {
            
		// Sanitize input 
            foreach ($_POST as $key => $value) {
                $_POST[$key] = strip_tags($value); 
            }

            $userObj = new User(); 
            $user = $userObj->authenticate($_POST['username'], $_POST['password']); 
            

            // Run the conditional.
            if (!empty($user)) {

                    // Set a session variable  as logged in 
                    $session->login($user['id']); 

                    // Redirect to index.php 
                    header('location:index.php'); 
            } else {
                $this->variables['error_message'] = 'That account does not exist.'; 
            }
        }
    }
}

$controller = new LoginController(); 
print $controller->run(); 