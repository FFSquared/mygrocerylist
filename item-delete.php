<?php 
include 'bootstrap.php';

class ItemDeleteController extends Controller {
    
    protected $layoutTemplate = 'layout-no-menu'; 
    
    protected $template = 'item-delete'; 
    
    protected $variables = array(); 
    
    public function preRender() {
        if (!empty($_GET['id'])) {
            $item = new Item(); 
            $item->delete($_GET['id']); 
        }
    }
}

$session->isAuthorized(); 
$controller = new ItemDeleteController();
print $controller->run(); 

