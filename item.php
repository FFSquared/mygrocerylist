<?php 
include 'bootstrap.php'; 

class ItemController extends Controller {

    protected $template = 'item'; 

    protected $variables = array(
        'error_message' => '',
        'name' => '',
        'qty' => '', 
    ); 

    public function preRender() {

        // Item object. 
        $item = new Item(); 
        
        // If query string is set 
        if (!empty($_GET['id'])) {

            // Select that row from the database 
            $data = $item->fetch($_GET['id']); 
            $this->variables['name'] = $data['name']; 
            $this->variables['qty'] = $data['qty'];
        }
        
        // Form submit handler 
        if (!empty($_POST)) {

            foreach($_POST as $key => $value) {
                // Sanitize input
                $value = strip_tags($value); 

                // Save sanitized post values into form array 
                // This array is used to populate the form after form submission. 
                $_POST[$key] = $value; 
            }

            if (!empty($_POST['name']) && !empty($_POST['qty'])) {

                // If query string is set then update record 
                if (!empty($_GET['id'])) {
            
                    $fields = array(
                        'name' => $_POST['name'],
                        'qty' => $_POST['qty']
                    ); 
                    $item->update($fields, $_GET['id']); 

                    header('location:list.php'); 

                // If ID is not set then insert record
                } else {
                    
                    // Insert new item. 
                   $fields = array(
                        'name' => $_POST['name'],
                        'qty' => $_POST['qty'],
                        'user_id' => $_SESSION['user_id'],
                    ); 
                    $item->insert($fields); 
                    
                    // Run query.
                    header('location:list.php'); 
                }
            } else {
                $this->variables['error_message'] = "Please provide some values.";
            }
        }
    }
}

$session->isAuthorized(); 
$controller = new ItemController(); 
print $controller->run(); 
