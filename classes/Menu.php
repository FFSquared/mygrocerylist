<?php 
class Menu {
    
    private $items = array(); 
    
    /**
    * Constructor.
    */ 
    public function __construct($items) {
        $this->items = $items; 
    }
    
    /**
    * Set the menu items.
    */ 
    public function set($items) {
        $this->items = $items; 
    } 
    
    /**
    * Get the menu.
    */ 
    public function get() {
        return $this->items; 
    }
    
    /**
    * Render the menu.
    */ 
    public function render() {
        if (!empty($this->items)) {
            $menu = '<ul>'; 
            // Loop through each item
            foreach ($this->items as $url => $title) {
                // Print that item as a link 
                $menu .= '<li><a href = "'.$url.'">'.$title.'</a></li>';
            }
            $menu .= '</ul>'; 
            
            return $menu; 
	   }
    }
}