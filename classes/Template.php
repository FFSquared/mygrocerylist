<?php 
class Template {
    
    protected $variables = array(); 
    
    protected $file; 
  
    public function __construct($file, $variables = array()) {
        $this->file = $file; 
        $this->variables = $variables; 
    }
    
    public function render() {
        $variables = $this->variables; 
        $search = array(); 
        $replace = array(); 
        
        $content = file_get_contents(BASEDIR . '/templates/' . $this->file . '.html'); 
        
        foreach ($variables as $key => $value) {
            $key = '{{ ' . $key . ' }}'; 
            $search[] = $key;
            $replace[] = $value; 
        }
        
        $content = str_replace($search, $replace, $content); 

        return $content;         
    }
}
