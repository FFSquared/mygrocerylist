<?php
// Config
include 'config.php'; 

include_once 'classes/Session.php'; 
include_once 'classes/Menu.php'; 
include_once 'classes/Template.php';
include_once 'classes/Controller.php'; 
include_once 'classes/Database.php'; 
include_once 'classes/Model.php'; 
include_once 'classes/Item.php'; 
include_once 'classes/User.php'; 

// Initialize the session class. 
$session = new Session(); 
$session->start(); 

